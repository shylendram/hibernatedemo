package com.geeklabs.pojo;

public class Employee {
	private int id;
	private String name;
	private String role;
	private int salary;
	
	public Employee() {
	}
	
	public Employee(String name, String role, int salary) {
		this.name = name;
		this.role = role;
		this.salary = salary;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	
}
