package com.geeklabs.common;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.geeklabs.pojo.Employee;

public class ManageEmployee {
	private static SessionFactory sessionFactory;

	public static void main(String[] args) {
		// way 1 to get session factory but deprecated
		/*
		 * SessionFactory buildSessionFactory = new
		 * Configuration().configure().buildSessionFactory();
		 */
		// way 2 to get session factory not deprecated use this only.
		Configuration configuration = new Configuration();
		configuration.configure();
		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties())
				.buildServiceRegistry();
		sessionFactory = configuration.buildSessionFactory(serviceRegistry);

		ManageEmployee employee = new ManageEmployee();
		// add employee
		Integer employeeId = employee.addEmployee("shylu", "android developer", 50000);
		// get employees
		employee.getEmployees();
		// update employee
		employee.updateEmployee(employeeId);
		// delete employee
		employee.deleteEmployee(employeeId);
	}

	// add employee
	private Integer addEmployee(String name, String role, int salary) {
		// open new session
		Session session = sessionFactory.openSession();
		Transaction txn = null;
		// get the transaction
		txn = session.beginTransaction();
		Employee employee = new Employee(name, role, salary);
		Integer id = (Integer) session.save(employee);
		System.out.println("Emplyee added successfully");

		// commit and close the session
		txn.commit();
		session.close();
		return id;
	}

	// get employees
	private void getEmployees() {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		List empList = session.createQuery("from Employee").list();
		for (Iterator iterator = empList.iterator(); iterator.hasNext();) {
			Employee employee = (Employee) iterator.next();
			System.out.println("Id:" + employee.getId());
			System.out.println("Name:" + employee.getName());
			System.out.println("Role:" + employee.getRole());
			System.out.println("Salary:" + employee.getSalary());
		}
		transaction.commit();
		session.close();
	}

	// update employee
	private void updateEmployee(Integer employeeId) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		Employee employee = (Employee) session.get(Employee.class, employeeId);
		employee.setSalary(60000);
		session.update(employee);
		System.out.println("Employee updated successfully");

		transaction.commit();
		session.close();
	}

	// delete employee
	private void deleteEmployee(Integer employeeId) {
		Session openSession = sessionFactory.openSession();
		Transaction beginTransaction = openSession.beginTransaction();
		Employee emp = (Employee) openSession.get(Employee.class, employeeId);
		openSession.delete(emp);
		System.out.println("Employee deleted successfully");

		beginTransaction.commit();
		openSession.close();
	}

}
